package com.example.robotapp

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.robotapp.Model.Data
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_disinfection.view.*
import kotlinx.android.synthetic.main.fragment_statistics.*
import kotlinx.android.synthetic.main.fragment_statistics.view.*


class StatisticsFragment : Fragment() {

    private lateinit var firebaseUser: FirebaseUser
    lateinit var barList: ArrayList<BarEntry>
    lateinit var barDataSet: BarDataSet
    lateinit var barData : BarData
     var tempArr: ArrayList<String> ?= null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_statistics, container, false)
        barList= ArrayList()


        val bundle = arguments
        if (bundle != null) {
            tempArr  = bundle.getSerializable("temperature") as ArrayList<String>?

        }

        for (it in tempArr!!)
        {
            var i=0f
            barList.add(BarEntry(i,it.toFloat()))
            barDataSet= BarDataSet(barList," Statistics")
            barData= BarData(barDataSet)
            view.bar_chart.data=barData
            view.bar_chart.setDrawGridBackground(false)
            view.bar_chart.xAxis.isEnabled = false
            view.bar_chart.axisLeft.isEnabled = false
            view.bar_chart.axisRight.isEnabled = false

            barDataSet.setColors(ColorTemplate.JOYFUL_COLORS,250)
            barDataSet.valueTextColor=Color.WHITE
            barDataSet.valueTextSize=15f
            i++
        }
        return view
    }

}